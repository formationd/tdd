package com.k.just.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.k.just.model.Perf;

@Component
public class JMSService {

    @JmsListener(destination = "TOPIC_PERF", containerFactory = "myFactory")
	public void receiveMessage(Perf message) {
		System.out.println("Received Perf <" + message.getTitle() + ">");
	}
}
