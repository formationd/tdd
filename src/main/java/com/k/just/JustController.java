package com.k.just;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.k.just.model.Perf;
import com.k.just.model.PerfFactory;
import com.k.just.service.DataService;
import com.k.just.service.FileService;
import com.k.just.service.PerfHttpService;
import com.k.just.service.PerfOutOfMemService;
import com.k.just.service.ThreadService;



@RestController
@RequestMapping("/")
public class JustController {

	@Autowired
	PerfOutOfMemService perfOutOfMemService;
	
	@Autowired
	PerfHttpService perfHttpService;

	@Autowired
	FileService fileService;

	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	ThreadService threadService;
	
	@Autowired
	DataService dataService;
	
	@GetMapping("/data")
	ResponseEntity<String> loadData() {
		String news = "OK Connect";
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		String errMsg;
		List<Perf> perf;

		try {
			perf = dataService.loadData();
			return new ResponseEntity(perf, HttpStatus.OK);
		} catch (Exception e) {
			errMsg = e.getMessage();
		}
		return new ResponseEntity(new Perf(new Date(), errMsg, ""), status);
	}
	
	
	
	@GetMapping("/")
	ResponseEntity<String> firstpage() {
		String news = "OK Connect";
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		String errMsg;
		Perf perf;

		try {
			perf = perfHttpService.getById("0");
			return new ResponseEntity(perf, HttpStatus.OK);
		} catch (Exception e) {
			errMsg = e.getMessage();
		}
		return new ResponseEntity(new Perf(new Date(), errMsg, ""), status);
	}

	// load file
	@GetMapping("/files")
	ResponseEntity<String> loadpage() {
		String ip = "NOK";
		try {
			ip = fileService.loadFileReader();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity(ip, HttpStatus.OK);

	}

	@GetMapping("/ocr")
	ResponseEntity<String> loadocr() {
		String ip = "NOK";
			ip = fileService.loadOCR();

		return new ResponseEntity(ip, HttpStatus.OK);

	}

	@GetMapping("/thr")
	ResponseEntity<String> loadThreads() {
		threadService.createLockThr();
		return new ResponseEntity("THEAD OK", HttpStatus.OK);
	}


	@GetMapping("/therror")
	ResponseEntity<String> loadThreadsError() {
		threadService.createLockThrError();
		return new ResponseEntity("ERROR", HttpStatus.OK);
	}


	@GetMapping("/jms")
	ResponseEntity<String> loadJMS() {
		jmsTemplate.convertAndSend("TOPIC_PERF", PerfFactory.createPerf());
		return new ResponseEntity("SEND MSG", HttpStatus.OK);
	}


	@GetMapping("/jmss")
	void loadJMS(@RequestBody Perf perf) {
		jmsTemplate.convertAndSend("TOPIC_PERF", PerfFactory.createPerf());
	}

	@GetMapping("/perfout")
	ResponseEntity<String> loadOutOf() {
		perfOutOfMemService.outOfMemory();
		return new ResponseEntity("TEST OUT OF MEMORY", HttpStatus.OK);
	}

	@GetMapping("/native")
	ResponseEntity<String> loadNative() {
		perfOutOfMemService.nativeException();
		return new ResponseEntity("TEST NATIVE ", HttpStatus.OK);
	}



}
