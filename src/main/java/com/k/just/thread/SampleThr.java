package com.k.just.thread;

import java.util.concurrent.locks.Lock;

public class SampleThr extends Thread {

    //private Semaphore semaphore;
    private Lock lock;
    private String name;

    public SampleThr(String n, Lock lock) {
        this.lock = lock;
        name = n;
    }


    public synchronized void createCompte() {
        
    }


    public void run() {
        try {
            //semaphore.acquire();
            lock.lock();
            System.out.println(name + " is running \n");
            sleep(60000);
            if (name.equals("THR2")) throw new InterruptedException();
            System.out.println(name + " is unlocked \n");
            lock.unlock();
            // deverouillage
            //semaphore.release();
        } catch (InterruptedException e) {
            System.out.println("An error occured " + e.getMessage());
        }
    }

}
