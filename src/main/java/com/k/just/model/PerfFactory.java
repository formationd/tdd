package com.k.just.model;

import java.util.Date;

public class PerfFactory {


    public static Perf createPerf() {
        return new Perf(new Date(), "title", "content");
    }


    public static Perf createPerf(String title, String content) {
        return new  Perf(new Date(), title, content);
    }

}
