package com.k.just.model;

import java.io.Serializable;
import java.util.Date;

public class Perf implements Serializable {

	private Date time;

	private String title;
	private String content;

	public Perf() {

	}


	public Perf(Date time, String title, String content) {
		this.time = time;
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Perf [time=" + time + ", title=" + title + ", content=" + content + "]";
	}

}
