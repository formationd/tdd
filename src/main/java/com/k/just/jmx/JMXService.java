package com.k.just.jmx;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

@Service
@ManagedResource(objectName = "spring:name=jMXService")
public class JMXService {

    private final Lock lock = new ReentrantLock();

    private String info = "valeur MBean";

    public synchronized Lock getLock() {
        return lock;
    }

    @ManagedAttribute
    public synchronized boolean unlock() {
        lock.unlock();
        return true;
    }

    @ManagedAttribute
    public synchronized String getInfo() {
        return info;
    }

    @ManagedAttribute
    public void setInfo(String info) {
        this.info = info;
    }


}
