package com.k.just.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.k.just.dao.DataStorageDao;
import com.k.just.model.Perf;
import com.k.just.model.PerfFactory;

@Service
public class DataStorageDaoImpl implements DataStorageDao {

	
	List<Perf> listPerf = new ArrayList<Perf>();
	
	@Override
	public List<Perf> loadData() {
		listPerf.add(PerfFactory.createPerf("title", "content"));
		listPerf.add(PerfFactory.createPerf("title1", "content1"));
		
		return listPerf;
	}

}
