package com.k.just.dao;

import java.util.List;

import com.k.just.model.Perf;


public interface DataStorageDao {
	
	
	public List<Perf> loadData();
	

}
