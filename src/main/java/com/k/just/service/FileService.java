package com.k.just.service;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Service;

import net.sourceforge.tess4j.Tesseract1;
import net.sourceforge.tess4j.TesseractException;

@Service
public class FileService {

    Map<Double, Double> list = new HashMap();

    public void addMap() {
        list.put(Math.random(), Math.random());

        for(Map.Entry<Double, Double> entry : list.entrySet()) {
            Double key = entry.getKey();
            Double value = entry.getValue();

        }

        list.put(Math.random(), Math.random());

    }

    List<Double> liste = new ArrayList<Double>();

    public void addList() {
        liste.add(Math.random());

        final Iterator<Double> it = liste.iterator();
        while (it.hasNext()) {
            Double type = (Double) it.next();
            if (type > 10.0) it.remove();
        }
    }

    public String load() throws IOException {
        ClassLoader c = Thread.currentThread().getContextClassLoader();

        InputStream in = c.getResourceAsStream("fjava.pdf");

        //StringBuilder stb = new StringBuilder();
        String stb = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = br.readLine()) != null) {
            //stb.append(line).append("\n");
            stb.concat(line).concat("\n");
        }
        return stb;
    }


    public String loadStream(String filePath) throws IOException {
        InputStream in = null;
        File file = new File(filePath);
        if (file.exists()) {
            in = new FileInputStream(file);
        } else {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath);
        }

        StringBuilder stb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = br.readLine()) != null) {
            stb.append(line).append("\n");
        }
        return stb.toString();

    }



    public String loadFileReader() throws IOException {
        //File file = new File("fjava.pdf");
        BufferedReader reader = new BufferedReader(new FileReader("src/main/resources/fjava.pdf"));

        StringBuilder stb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stb.append(line).append("\n");
        }

        reader.close();



        return stb.toString();
    }

    public String loadOCR() {
        Tesseract1 instance = new Tesseract1();
        instance.setDatapath("tmp/tessdata-master");
        instance.setLanguage("eng");
        instance.setTessVariable("user_defined_dpi", "140");

        System.out.println(">> " + Locale.getDefault());

        try {
            String result = instance.doOCR(new File("src/main/resources/fjava.pdf"));
            return result;
        } catch (TesseractException e) {
            e.printStackTrace();
        }
        return "RESULT KO";
    }


}
