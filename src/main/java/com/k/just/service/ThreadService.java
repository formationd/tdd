package com.k.just.service;

import java.util.concurrent.locks.Lock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.k.just.jmx.JMXService;
import com.k.just.thread.SampleThr;

@Service
public class ThreadService {

    @Autowired
    private JMXService jmxService;

    /**
     * URL - test JMETER
     */
    public void createLockThr() {
        Lock lock = jmxService.getLock();
        Thread sampleThr = new SampleThr("THR", lock);
        sampleThr.start();
    }

    /**
     * URL - navigateur
     */
    public void createLockThrError() {
        Lock lock = jmxService.getLock();
        for (int i = 0; i < 10 ; i++) {
            Thread sampleThr = new SampleThr("THR" + i, lock);
            sampleThr.start();
        }

    }

}
