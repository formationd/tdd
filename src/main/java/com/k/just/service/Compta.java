package com.k.just.service;

public class Compta {
	
	
	public int calculTTC(int tva, int value) {
		if (tva != 10 && tva!= 20) throw new IllegalArgumentException("TVA_ERROR");
		if (value == 0)  throw new IllegalArgumentException("VALUE_ERROR");
		
		//res * (tva/100 +1)
		double a = 20/100 + 1;
		
		return (int) (value * 1.2);
		
		
	}

}
