package com.k.just.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.k.just.dao.DataStorageDao;
import com.k.just.model.Perf;

@Service
public class DataService {
	
	
	@Autowired
	private DataStorageDao dataStorageDao;
	
	
	public List<Perf> loadData() {
		return dataStorageDao.loadData();
	}

}
