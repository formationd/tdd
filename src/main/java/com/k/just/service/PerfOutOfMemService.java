package com.k.just.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.k.just.jmx.JMXService;
import com.k.just.model.Perf;
import com.k.just.model.PerfFactory;
import com.k.just.thread.SampleThr;

@Component
public class PerfOutOfMemService {

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private JMXService jmxService;

	private List<Perf> perfList = new ArrayList<Perf>();

	/**
	 * createObjects
	 * @return
	 */
	public List<Perf> createObjects() {
		List<Perf> perfs = new ArrayList<Perf>();
		for (int i = 0; i < 10000000; i++) {
			Perf p = new Perf(new Date(), "title", "content"+i);
			perfs.add(p);
		}
		return perfs;
	}
	
	
	
	public void threadRunning() {
		
		
	}


	public void outOfMemory() {
		for (int i = 0; i < 1000000 ; i++) {
			Perf p = PerfFactory.createPerf();
			perfList.add(p);
			//jmsTemplate.convertAndSend("TOPIC_PERF", "new objet");
		}
	}


	public void nativeException() {
		for (int i = 0; i < 10000 ; i++) {

			Thread sampleThr = new SampleThr("THR", jmxService.getLock());
			sampleThr.start();


		}
	}

}
