package com.k.just.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.k.just.model.Perf;

@Component
public class PerfHttpService {

	private String resource = "https://fr.news.yahoo.com/";

	private String get(String id) throws Exception {

		HttpURLConnection connection = (HttpURLConnection) (new URL(this.resource)).openConnection();

		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		StringBuilder response = new StringBuilder();

		String line = reader.readLine();
		while (line != null) {
			response.append(line);
			line = reader.readLine();
		}

		connection.disconnect();

		return response.toString();

	}

	private Document getDocument(String xmlResponde) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		factory.setNamespaceAware(true);

		DocumentBuilder builder;

		Document doc = null;

		builder = factory.newDocumentBuilder();

		doc = builder.parse(new InputSource(new StringReader(xmlResponde)));

		return doc;

	}

	private Perf newsBuilder(String xmlResponde) throws Exception {

		Document doc = getDocument(xmlResponde);

		XPath xpath = XPathFactory.newInstance().newXPath();

		XPathExpression expr = xpath.compile("/");
		String title = (String) expr.evaluate(doc, XPathConstants.STRING);

		expr = xpath.compile("/");
		String content = (String) expr.evaluate(doc, XPathConstants.STRING);

		return new Perf(new Date(), title, content);

	}

	public Perf getById(String id) throws Exception {
		return this.newsBuilder(this.get(id));
	}

}
