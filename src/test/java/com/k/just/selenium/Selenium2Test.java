package com.k.just.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Selenium2Test {

	 private WebDriver driver;		
		@Test				
		public void testEasy() {	
			driver.get("http://www.orsys.com");  
			String title = driver.getTitle();				 
			Assert.assertTrue(title.contains("orsys")); 		
		}	
		@BeforeTest
		public void beforeTest() {	
//		    driver = new FirefoxDriver();  
//		    driver = new ChromeDriver();
			
			
			System.setProperty("webdriver.chrome.driver", "operadriver");
			ChromeOptions options = new ChromeOptions();
			options.setBinary("/usr/bin/opera");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			//driver = new ChromeDriver(capabilities);
		    driver = new OperaDriver(capabilities);
		}		
		@AfterTest
		public void afterTest() {
			driver.quit();			
		}
}
