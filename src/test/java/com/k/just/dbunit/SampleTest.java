//package com.k.just.dbunit;
//
//import java.io.FileInputStream;
//
//import org.dbunit.DBTestCase;
//import org.dbunit.PropertiesBasedJdbcDatabaseTester;
//import org.dbunit.database.DatabaseConfig;
//import org.dbunit.dataset.IDataSet;
//import org.dbunit.dataset.xml.FlatXmlDataSet;
//
//public class SampleTest extends DBTestCase
//{
//    public SampleTest(String name)
//    {
//        super( name );
//        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "org.hsqldb.jdbcDriver" );
//        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "jdbc:hsqldb:sample" );
//        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, "sa" );
//        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, "" );
//	// System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, "" );
//    }
//
//    protected IDataSet getDataSet() throws Exception
//    {
//        return new FlatXmlDataSet(new FileInputStream("dataset.xml"));
//    }
//    
//    /**
//     * Override method to set custom properties/features
//     */
//    protected void setUpDatabaseConfig(DatabaseConfig config) {
//        config.setProperty(DatabaseConfig.PROPERTY_BATCH_SIZE, new Integer(97));
//        config.setFeature(DatabaseConfig.FEATURE_BATCHED_STATEMENTS, true);
//    }
//   
//}
