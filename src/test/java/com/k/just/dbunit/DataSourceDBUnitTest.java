//package com.k.just.dbunit;
//
//import java.io.InputStream;
//import java.sql.Connection;
//
//import javax.sql.DataSource;
//
//import org.dbunit.DataSourceBasedDBTestCase;
//import org.dbunit.dataset.IDataSet;
//import org.dbunit.dataset.ITable;
//import org.dbunit.dataset.xml.FlatXmlDataSet;
//import org.dbunit.operation.DatabaseOperation;
//import org.junit.Test;
//
//public class DataSourceDBUnitTest extends DataSourceBasedDBTestCase {
//
//	@Override
//	protected DataSource getDataSource() {
//		DataSource dataSource = new DataSourceDBUnitTest();
//		dataSource.setURL("jdbc:h2:mem:default;DB_CLOSE_DELAY=-1;init=runscript from 'classpath:schema.sql'");
//		dataSource.setUser("sa");
//		dataSource.setPassword("sa");
//		return dataSource;
//	}
//
//	@Override
//	protected IDataSet getDataSet() throws Exception {
//		return new FlatXmlDataSet(getClass().getClassLoader().getResourceAsStream("data.xml"));
//	}
//
//	@Override
//	protected DatabaseOperation getSetUpOperation() {
//		return DatabaseOperation.REFRESH;
//	}
//
//	@Override
//	protected DatabaseOperation getTearDownOperation() {
//		return DatabaseOperation.DELETE_ALL;
//	}
//
//	@Test
//	public void givenDataSetEmptySchema_whenDataSetCreated_thenTablesAreEqual() throws Exception {
//		IDataSet expectedDataSet = getDataSet();
//		ITable expectedTable = expectedDataSet.getTable("CLIENTS");
//		IDataSet databaseDataSet = getConnection().createDataSet();
//		ITable actualTable = databaseDataSet.getTable("CLIENTS");
//		assertEquals(expectedTable, actualTable);
//	}
//
//	@Test
//	public void givenDataSet_whenInsert_thenTableHasNewClient() throws Exception {
//		try (InputStream is = getClass().getClassLoader().getResourceAsStream("dbunit/expected-user.xml")) {
//			IDataSet expectedDataSet = new FlatXmlDataSet(is);
//			ITable expectedTable = expectedDataSet.getTable("CLIENTS");
//			Connection conn = getDataSource().getConnection();
//
//			conn.createStatement()
//					.executeUpdate("INSERT INTO CLIENTS (first_name, last_name) VALUES ('John', 'Jansen')");
//			ITable actualData = getConnection().createQueryTable("result_name",
//					"SELECT * FROM CLIENTS WHERE last_name='Jansen'");
//
//			assertEqualsIgnoreCols(expectedTable, actualData, new String[] { "id" });
//		}
//	}
//
//}
