package com.k.just.whitebox;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import com.k.just.service.Compta;

public class ComptaNodeFraisTest {

	
	Compta compta = new Compta();
	
	@Test
	void testCalculTTC_tva_error() {
		try {
			compta.calculTTC(21,100);
			fail("throw exception expected");
		} catch (IllegalArgumentException e) {
			assertEquals("TVA_ERROR", e.getMessage());
		}
		
	}

}
