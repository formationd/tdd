/**
 * 
 */
package com.k.just.whitebox;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.k.just.service.Compta;

/**
 * @author kode
 *
 */
class ComptaTest {

	Compta compta = new Compta();
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeMethod
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterMethod
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.k.just.service.Compta#calculTTC(int, int)}.
	 */
	@Test
	void testCalculTTC() {
		AssertJUnit.assertEquals(120, compta.calculTTC(20,100));
	}

	
	@Test
	void testCalculTTC_tva_error() {
		try {
			compta.calculTTC(21,100);
			Assert.fail("throw exception expected");
		} catch (IllegalArgumentException e) {
			AssertJUnit.assertEquals("TVA_ERROR", e.getMessage());
		}
		

		//assertThrows(IllegalArgumentException.class, compta.calculTTC(21,100));

	}
	
	@Test
	void testCalculTTC_value_error() {
		try {
			compta.calculTTC(20,0);
			Assert.fail("throw exception expected");
		} catch (IllegalArgumentException e) {
			AssertJUnit.assertEquals("VALUE_ERROR", e.getMessage());
		}
		
	}
}
