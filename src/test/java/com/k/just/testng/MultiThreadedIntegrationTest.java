package com.k.just.testng;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

public class MultiThreadedIntegrationTest {
  @Test
  public void f() {
  }
  
  @Test(threadPoolSize = 5, invocationCount = 10, timeOut = 1000)
  public void givenMethod_whenRunInThreads_thenCorrect() {
      int count = Thread.activeCount();
      assertTrue(count > 1);
  }

}
