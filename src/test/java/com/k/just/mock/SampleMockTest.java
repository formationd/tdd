package com.k.just.mock;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.k.just.dao.DataStorageDao;
import com.k.just.dao.impl.DataStorageDaoImpl;
import com.k.just.model.Perf;
import com.k.just.model.PerfFactory;
import com.k.just.service.DataService;


@RunWith(MockitoJUnitRunner.class)
public class SampleMockTest {

	@Mock
	private DataStorageDao mock;
	
	//service qui va recevoir le mock
	@InjectMocks
	private DataService dataService;
	List<Perf> perfsExpected = new ArrayList<Perf>(); 

	
	@Test
	public void testA( ) {
		
		Mockito.when(mock.loadData()).thenReturn(new ArrayList<Perf>(perfsExpected));
		
		
		List<Perf> perfs = dataService.loadData();
		
		perfs.add(PerfFactory.createPerf());
		
		assertEquals(perfsExpected.size(), perfs.size());
		
		
	}
	
	@Test
	public void testB( ) {
		
		DataService dataService = new DataService();
		DataStorageDao daStorageDaoMock = Mockito.mock(DataStorageDaoImpl.class);
		Mockito.when(daStorageDaoMock.loadData()).thenReturn(perfsExpected);
		//ReflectionUtils.setField(dataService, "dataStorageDao", daStorageDaoMock);
		
		//dataService.loadData();
		
	}
	
}
